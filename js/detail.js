$(document).ready(function () {
    var src_img_thumb, src_img_first;
    //hthi show mặc định
    src_img_first = $('ul#list-thumb li:first-child a img').attr('src');
    //gán src==>src_img_first
    $('#show img').attr('src', src_img_first);


    $('ul#list-thumb li a').click(function () {
        // lấy src-thumb
        src_img_thumb = $(this).children('img').attr('src');
        //src (thumb==>show)
        $('#show img').attr('src', src_img_thumb);

        return false;
    });

    $('ul#list-thumb li:first-child a img').addClass('active');
    $('ul#list-thumb li a img').click(function () {
        //active
        if (!$(this).hasClass('active')) {
            $('ul#list-thumb li a img').removeClass('active');
            $(this).addClass('active');
        }
    });

});
