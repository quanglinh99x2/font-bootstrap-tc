$(document).ready(function(){
    $('.fa-plus').hide( );
    $(window).on('resize', function(){
        var win = $(this); //this = window
        // if (win.height() >= 820) { /* ... */ } 
        if (win.width() >= 768) { 
          $('.menu-collapse').removeClass("collapse");
          $('.menu-collapse').addClass("collapse-show");
          $('.fa-plus').hide( );
         }
         else if (win.width() <746) {
          $('.menu-collapse').removeClass("collapse-show");
          $('.menu-collapse').addClass("collapse");
          $('.fa-plus').show( );
         }
    });
  });